var TableDatatablesEditable = function () {

    var handleTable = function () {
        $.datepicker.setDefaults("dateFormat", "yy-mm-dd");

        function register_datepicker() {
            jQuery('.datepicker').datepicker({
                autoSize: true,
                changeMonth: true
            });
        }

        function restoreRow(oTable, nRow) {
            if(null !== nRow) {
                var tr = jQuery(nRow);
                var aData = oTable.fnGetData(nRow);
                oTable.fnUpdate(aData.description, nRow, 1, false);
                oTable.fnUpdate(aData.amount, nRow, 2, false);
                oTable.fnUpdate(aData.date, nRow, 3, false);
                jQuery(tr).html(expenseTrTemplate.render(aData));
            }
            oTable.fnDraw();
            oTable.api(true).responsive.rebuild();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var html = expenseFormTemplate.render(aData);
            var tr = jQuery(nRow);
            tr.html('<td colspan="4"><p>Please, fill out the form bellow</p>'+html+'</td>');
            jQuery('td:first-child', nRow).addClass('edit-in-progess');
        }

        function saveRow(oTable, nRow) {
            var tr = jQuery(nRow);
            var jqInputs = jQuery('input', nRow);
            var aData = {
                description: jqInputs[0].value,
                amount: jqInputs[1].value,
                date: jqInputs[2].value
            };
            oTable.fnUpdate(aData.description, nRow, 1, false);
            oTable.fnUpdate(aData.amount, nRow, 2, false);
            oTable.fnUpdate(aData.date, nRow, 3, false);
            jQuery(tr).html(expenseTrTemplate.render(aData));
            oTable.fnDraw();
        }

        var addActionsButtons = function(){
            return '<div class="text-left">'
                    + '<button type="button" class="btn btn-edit btn-warning btn-sm" data-action="edit"><i class="fa fa-edit"></i> Edit</button>'
                    + '<span>&nbsp;|&nbsp;</span>'
                    + '<button type="button" class="btn btn-delete btn-danger btn-sm" data-action="delete"><i class="fa fa-trash-o"></i> Delete</button>'
                + '</div>'
            ;
        };

        var expenseFormTemplate = $.templates("#expenseFormTemplate");
        var expenseTrTemplate = $.templates('#expenseRowTemplate');

        var table = $('#expensesList');

        var oTable = table.dataTable({
            "responsive": true,
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>><'row'<'col-xs-12'r><'col-xs-12't>><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "ajax": {
                "url":"js/expenses-data.json",
                "dataSrc": "" //TODO: add a coma when uncomment next lines
                // "url":"http://interview.businessfacilitation.org/api/expenses",
                // "crossDomain":true,
                // "headers":{
                //     "Content-Type": "application/json; charset=utf-8",
                //     // Taken from: https://stackoverflow.com/questions/5584923/a-cors-post-request-works-from-plain-javascript-but-why-not-with-jquery
                //     "Access-Control-Allow-Origin":"x-requested-with, x-requested-by",
                //     "Access-Control-Allow-Methods": "GET",
                //     "Access-Control-Max-Age": 86400,
                //     "Control-Allow-Headers": "Content-Type"
                // },
                // "contentType": 'application/json; charset=UTF-8'
                // "dataType": "text"
            },

            "lengthMenu": [[1, 5, 10, 25, 50, 75, 100, -1], [1, 5, 10, 25, 50, 75, 100, "all"]],
            "pageLength": 10,
            "language": {
                "decimal": ".",
                "thousands": ",",
                "infoEmpty": "No records found to show",
                "emptyTable": "No data available in table",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "next": "<i class='fa fa-angle-right'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>"
                }
            },
            "processing": true,
            "deferRender": true,
            "serverSide": false,
            "searching": true,
            "paging": true,
            "order": [ 0, "asc" ],
            "columns":[
                {"data":"id", "className":"never"},
                {"data":"description", "className":"all"},
                {"data":"amount", "className":"all", "class":"text-right"},
                {"data":"date", "className":"not-mobile", "class":"text-center", "searchable":false},
//              {"data":null, className:"not-mobile", "searchable":false}, //TODO: Commented 'cause in the api doesn't exist
                {"data":null, "className":"not-mobile", "class":"text-left", "sortable":false, "searchable":false, "defaultContent":addActionsButtons()}
            ],
            "columnDefs": [{ "visible": false,  "targets": [ 0 ] }],
        });

        var oTableApi = oTable.api(true);

        var tableWrapper = $("#expensesList_wrapper");

        var nEditing = null;
        var nNew = false;

        $('#addExpense').click(function (e) {
            e.preventDefault();

            if (nNew && null !== nEditing) {
                if( confirm("Previous row not saved. Do you want to save it ?") ) {
                    saveRow(oTable, nEditing); // save
                    nEditing = null;
                    nNew = false;
                } else if( jQuery(nEditing).hasClass('new-expense') ){
                    return;
                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;

                    return;
                }
            } else if( null !== nEditing && jQuery(nEditing).hasClass('new-expense') ) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
            } else if( null !== nEditing ) {
                if( confirm("Previous row not saved. Do you want to save it ?") ) {
                    saveRow(oTable, nEditing); // save
                    // $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            }else{
                // nothing for now
            }

            var aiNew = oTable.fnAddData({"id":"", "description":"", "amount":"", "date":""});
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            jQuery(nRow).addClass('new-expense');
            nEditing = nRow;
            nNew = true;
            register_datepicker();
        });

        table.on('click', '.btn-delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") === false) {
                return;
            }

            var td = jQuery(this).closest('td');
            var tr = td.closest('tr');
            var row = tr[0];
            var expense = row.data();
            jQuery.ajax('http://interview.businessfacilitation.org/api/expenses/'+expense.id, {
                method: 'DELETE',
                crossDomain:true,
                complete: function(xhr, status){
                    // if(status === "success"){
                        oTable.fnDeleteRow(row);
                        alert("Deleted!");
                    // }
                }
            });
        });

        table.on('click', '.btn-cancel', function (e) {
            e.preventDefault();
            var row = jQuery(nEditing);
            if ( ( nNew && row.hasClass('new-expense') ) || row.hasClass('new-expense') ) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.btn-edit', function (e) {
            e.preventDefault();

            $this = jQuery(this);
            /* Get the row as a parent of the link that was clicked on */
            var nRow = $this.parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                var trChild = jQuery(nRow);
                if(trChild.hasClass('child')){
                    var trParent = trChild.prev('.parent');
                    nRow = trParent[0];
                    trChild.hide();
                }
                editRow(oTable, nRow);
                nEditing = nRow;
                register_datepicker();
            } else if (nEditing == nRow && $this.data('action') == "save") {
                /* Editing this row and want to save it */
                // jQuery.ajax('http://interview.businessfacilitation.org/api/expenses/'+expense.id, {
                //     method: 'DELETE',
                //     crossDomain:true,
                //     complete: function(xhr, status){
                //         if(status === "success"){
                //             oTable.fnDeleteRow(row);
                //             alert("Deleted!");
                //         }
                //     }
                // });
                // var jqInputs = $('input', nEditing);
                saveRow(oTable, nEditing);
                nEditing = null;
                alert("Updated!");
            } else {
                /* No edit in progress - let's start one */
                var trChild = jQuery(nRow);
                if(trChild.hasClass('child')){
                    var trParent = trChild.prev('.parent');
                    nRow = trParent[0];
                    trChild.hide();
                }
                editRow(oTable, nRow);
                nEditing = nRow;
                register_datepicker();
            }
        });
    };

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable.init();
});